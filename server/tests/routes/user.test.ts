import request from 'supertest';
import server from '../../src';

describe('users', () => {
  it('should get users for GET /users', done => {
    return request(server)
      .get('/users')
      .expect(200, done);
  });
});
