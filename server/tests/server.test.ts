import request from 'supertest';
import server from '../src';

describe('app', () => {
  it('should not respond to GET /', done => {
    return request(server)
      .get('/')
      .expect(404, done);
  });
});
